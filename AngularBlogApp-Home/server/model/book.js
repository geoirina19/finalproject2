const mongoose = require('mongoose');
const Schema = mongoose.Schema;
 
// create a schema
const bookSchema = new Schema({
  id: { type: Number, required: true, unique: true },
  name: { type: String, required: true },
  author: { type: String, required: true },
  price: { type: Number, required: true }
}, { collection : 'books' });
 
const Book = mongoose.model('Book', bookSchema);
 
module.exports = Book;