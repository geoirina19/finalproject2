const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const mongoose = require('mongoose');
const url = 'mongodb+srv://ira:ira@cluster0-npljt.mongodb.net/usersdb?retryWrites=true';
const urlBooks = 'mongodb+srv://ira:ira@cluster0-npljt.mongodb.net/booksdb?retryWrites=true';

const User = require('./model/user');
const Book = require('./model/book');

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended : false}))

app.post('/api/user/login', (req, res) => {
	console.log("iii");
	mongoose.connect(url,{useNewUrlParser: true}, function(err){
		if(err) throw err;
		console.log("iii connected");
		User.find({
			username : req.body.username, password : req.body.password
		}, function(err, user){
			if(err) throw err;

			if(user.length === 1){
				console.log("iii found");
				return res.status(200).json({
					status: 'success',
					data: user
				})
			} else {
				console.log("iii not found");
				return res.status(200).json({
					status: 'fail',
					message: 'Login Failed'
				})
			}
			
		})
	});
})

app.get('/api/books', (req, res) => {
	let page = parseInt(req.query.page);
	mongoose.connect(urlBooks,{useNewUrlParser: true}, function(err){
		if(err) throw err;
		console.log("iii connected");
		Book.find({}, {}, {skip: 5 * page, limit: 5}, function(err, books){
			if(err) throw err;

			//if(books.length  > 0){
				console.log("iii found");
				console.log(books);
				return res.status(200).json({
					status: 'success',
					data: books
				})
			/*} else {
				console.log("iii not found");
				return res.status(200).json({
					status: 'fail',
					message: 'no books'
				})
			}*/
			
		})
	});
})

app.post('/api/user/create', (req, res) => { //
	mongoose.connect(url, function(err){
		if(err) throw err;
		const user = new User({
			name: req.body.name,
			username: req.body.username,
			password: req.body.password
		})
		user.save((err, res) => {
			if(err) throw err;
			return res.status(200).json({
				status: 'success',
				data: res
			})
		})
	});
})

app.listen(3000, () => console.log('blog server running on port 3000!'))