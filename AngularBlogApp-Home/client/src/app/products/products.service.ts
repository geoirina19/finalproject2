import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';


@Injectable()
export class ProductService {

	constructor(private http: HttpClient){

	}
	getBooks(page: number){
		const params = new HttpParams().set('page', String(page));
		const options = {params: params};
		return this.http.get('/api/books', options);
	}

}